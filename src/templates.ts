import { ITemplateUnit } from './unit';

const DEPTH_TO_WIDTH = 3

export const templates = {
    BattalionAEast: [
        { x: 50 , y: DEPTH_TO_WIDTH * 0  , type: 'recce'                   , size: 'platoon', additional: null },
        { x: 100, y: DEPTH_TO_WIDTH * 33 , type: 'mechanised infantry'     , size: 'company', additional: null },
        { x: 0  , y: DEPTH_TO_WIDTH * 33 , type: 'mechanised infantry'     , size: 'company', additional: null },
        { x: 100, y: DEPTH_TO_WIDTH * 66 , type: 'tank'                    , size: 'platoon', additional: 'T-72' },
        { x: 0  , y: DEPTH_TO_WIDTH * 66 , type: 'tank'                    , size: 'platoon', additional: 'T-72' },
        { x: 50 , y: DEPTH_TO_WIDTH * 70 , type: 'self-propelled artillery', size: 'company', additional: null },
        { x: 100, y: DEPTH_TO_WIDTH * 70 , type: 'mechanised infantry'     , size: 'company', additional: null },
        { x: 70 , y: DEPTH_TO_WIDTH * 70 , type: 'air defense'             , size: 'platoon', additional: null },
        { x: 50 , y: DEPTH_TO_WIDTH * 78 , type: 'hq'                      , size: 'battalion', additional: null },
        { x: 50 , y: DEPTH_TO_WIDTH * 89 , type: 'tank'                    , size: 'platoon', additional: 'T-72' },
        { x: 50 , y: DEPTH_TO_WIDTH * 100, type: 'supply'                  , size: 'company', additional: null },
    ] as ITemplateUnit[],
    BattalionAWest: [
        { x: 50 , y: DEPTH_TO_WIDTH * 0  , type: 'recce'                   , size: 'platoon', additional: null },
        { x: 100, y: DEPTH_TO_WIDTH * 33 , type: 'mechanised infantry'     , size: 'company', additional: null },
        { x: 0  , y: DEPTH_TO_WIDTH * 33 , type: 'mechanised infantry'     , size: 'company', additional: null },
        { x: 100, y: DEPTH_TO_WIDTH * 66 , type: 'tank'                    , size: 'platoon', additional: 'T-72' },
        { x: 0  , y: DEPTH_TO_WIDTH * 66 , type: 'tank'                    , size: 'platoon', additional: 'T-72' },
        { x: 50 , y: DEPTH_TO_WIDTH * 70 , type: 'self-propelled artillery', size: 'company', additional: null },
        { x: 20 , y: DEPTH_TO_WIDTH * 70 , type: 'mechanised infantry'     , size: 'company', additional: null },
        { x: 70 , y: DEPTH_TO_WIDTH * 70 , type: 'air defense'             , size: 'platoon', additional: null }, 
        { x: 50 , y: DEPTH_TO_WIDTH * 78 , type: 'hq'                      , size: 'battalion', additional: null },
        { x: 50 , y: DEPTH_TO_WIDTH * 89 , type: 'tank'                    , size: 'platoon', additional: 'T-72' },
        { x: 50 , y: DEPTH_TO_WIDTH * 100, type: 'supply'                  , size: 'company', additional: null },
    ] as ITemplateUnit[],
    BattalionBWest: [
        { x: 50 , y: DEPTH_TO_WIDTH * 0  , type: 'recce'                   , size: 'platoon', additional: null },
        { x: 100, y: DEPTH_TO_WIDTH * 33 , type: 'tank'                    , size: 'company', additional: 'T-90' },
        { x: 0  , y: DEPTH_TO_WIDTH * 33 , type: 'tank'                    , size: 'company', additional: 'T-90' },
        { x: 100, y: DEPTH_TO_WIDTH * 66 , type: 'mechanised infantry'     , size: 'platoon', additional: null },
        { x: 0  , y: DEPTH_TO_WIDTH * 66 , type: 'mechanised infantry'     , size: 'platoon', additional: null },
        { x: 50 , y: DEPTH_TO_WIDTH * 70 , type: 'self-propelled artillery', size: 'company', additional: null },
        { x: 20 , y: DEPTH_TO_WIDTH * 70 , type: 'tank'                    , size: 'platoon', additional: 'T-90' },
        { x: 70 , y: DEPTH_TO_WIDTH * 70 , type: 'air defense'             , size: 'platoon', additional: null },
        { x: 50 , y: DEPTH_TO_WIDTH * 78 , type: 'hq'                      , size: 'battalion', additional: null },
        { x: 50 , y: DEPTH_TO_WIDTH * 89 , type: 'mechanised infantry'     , size: 'company', additional: null },
        { x: 50 , y: DEPTH_TO_WIDTH * 100, type: 'supply'                  , size: 'company', additional: null },
    ] as ITemplateUnit[],
}
