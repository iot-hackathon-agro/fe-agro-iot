import { IReportedUnit, ITemplateUnit } from './unit'
import { getLength } from 'ol/sphere'
import LineString from 'ol/geom/LineString'
import { fromProjection, EARTH_RADIUS } from './constants'
import { templates } from './templates'

export const predict = (reportedUnits: IReportedUnit[]): IReportedUnit[] => {

    const predictedUnits: IReportedUnit[] = []
    
    const ghostUnitTemplateAsArray = Object.entries(templates) as [keyof typeof templates, ITemplateUnit[]][]
    // Iterate over templates.
    for(const [title, template] of ghostUnitTemplateAsArray) {

        // Skip templates that doesn't contain reported units.
        const ok = reportedUnits.every((reportedUnit) => {
            const matches = template.some(IsEqualUnit(reportedUnit))
            console.log(`${title}: ${matches ? '' : 'NOT'} found - ${reportedUnit.type} ${reportedUnit.size} (${reportedUnit.additional})`)
            return matches
        })
        if(!ok) continue

        // Select recce as reference unit. Assume it is reported, otherwise skip.
        const refReportedUnit = reportedUnits.find((reportedUnits) => reportedUnits.type === 'recce')
        if(!refReportedUnit) continue

        // Select random 2nd unit for rotation and scale.
        const [otherReportedUnit, ...restReportedUnits] = reportedUnits
            .filter((reportedUnits) => reportedUnits.type !== 'recce')
        if(!otherReportedUnit) continue

        // FYI: Beyond 2 units other's are ignored.

        const matchedOtherTemplateUnits = template
            .filter(IsEqualUnit(otherReportedUnit))
        console.log(matchedOtherTemplateUnits)

        // Iterate over matched units in template (e.g. 3 tanks).
        for(const [index, otherTemplateUnit] of matchedOtherTemplateUnits.entries()) {
            const refCoords = refReportedUnit.location
            const otherCoords = otherReportedUnit.location

            const refTemplateCoords: [number, number] = [50, 0]  // Hardcode recce position refTemplateCoords
            const otherTemplateCoords: [number, number] = [otherTemplateUnit.x, otherTemplateUnit.y]

            // Calculate scale. FYI: reportedDistance from recce to back tank: ~5700m
            const templateDistance = pythagorDistance(refTemplateCoords, otherTemplateCoords)
            const reportedDistance = getLength(new LineString([refCoords, otherCoords]), {projection: fromProjection, radius: EARTH_RADIUS})
            const k = reportedDistance / templateDistance
            console.log('k', reportedDistance, templateDistance, k)
            if(k < 20) continue  // below 9km / 1.3
            if(k > 35) continue  // above 9km * 1.3


            // Calculate template rotation. FYI: bearing from recce to back tank: ~3.6 rad / ~206 deg
            const bearingRefToOther = getBearingByTan(refCoords, otherCoords)
            const bearingTemplateRefToOther = getBearingByAtan2(refTemplateCoords, otherTemplateCoords) || 0
            const bearingDelta = bearingRefToOther - bearingTemplateRefToOther
            // console.log([
            //     `bearing field: ${bearingRefToOther} (${toDegrees(bearingRefToOther)})`,
            //     `bearing doctrine: ${bearingTemplateRefToOther} (${toDegrees(bearingTemplateRefToOther)})`,
            //     `bearing delta: ${bearingDelta} (${toDegrees(bearingDelta)})`,
            // ].join('\n'))

            // Using scale and bearingDelta, translate template location to map location.
            const thisTemplatePredictedUnits = template
                .map((templateUnit): IReportedUnit => {
                    const templateCoords: [number, number] = [templateUnit.x, templateUnit.y]
                    const bearing = getBearingByAtan2(refTemplateCoords, templateCoords) || 0
                    const location = offset(
                            refCoords,
                            k * pythagorDistance(refTemplateCoords, templateCoords),
                            bearingDelta + bearing,
                        )
                    // console.log([
                    //     `${templateUnit.type} ${templateUnit.size} (${templateUnit.additional})`,
                    //     `- doctrine:`,
                    //     `   - location: ${[templateUnit.x, templateUnit.y]}`,
                    //     `   - bearing from recce: ${bearing} (${toDegrees(bearing)})`,
                    //     `- field:`,
                    //     `   - location: ${location}`,
                    //     `   - bearing from recce: ${bearingDelta + bearing} (${toDegrees(bearingDelta + bearing)})`,
                    // ].join('\n'))
                    return {
                        activity: 'stationary',
                        additional: templateUnit.additional,
                        alignment: 'foe',
                        location,
                        size: templateUnit.size,
                        timestamp: 0,
                        type: templateUnit.type,
                    }
                })

            // Eliminate template variant if any of rest reported units doesn't match.
            const ok2 = restReportedUnits.every((reportedUnit) => {
                const templateUnits = thisTemplatePredictedUnits.filter(IsEqualUnit(reportedUnit))
                return !templateUnits.every((templateUnit) => {
                    const distance = getLength(new LineString([reportedUnit.location, templateUnit.location]), {projection: fromProjection, radius: EARTH_RADIUS})
                    console.log(distance)
                    return distance > 1000
                })
            })
            if(restReportedUnits.length > 0 && !ok2) continue

            // console.log('templateUnits', templateUnits)

            // console.log('templateUnitsGeojson', templateUnitsGeojson)

            predictedUnits.push(...thisTemplatePredictedUnits);

            console.info(`${title} ${index}: Displayed`)
            // break
        }


        console.info(`${title}: OK`, refReportedUnit)
        // break  // Break because show only 1 template.

        return predictedUnits
    }
}












































const IsEqualUnit = (reportedUnit: IReportedUnit) => (templateUnit: ITemplateUnit | IReportedUnit) => {
    return true
        && reportedUnit.type === templateUnit.type
        && reportedUnit.size === templateUnit.size
        && (templateUnit.additional === null || templateUnit.additional === reportedUnit.additional)
}


/**
 * Converts degrees to radians.
 * @param {number} angleDegrees Angle in degrees.
 * @return {number} Angle in radians.
 */
const toRadians = (angleDegrees: number) => {
  return angleDegrees * Math.PI / 180;
};


/**
 * Converts radians to degrees.
 * @param {number} angleRadians Angle in radians.
 * @return {number} Angle in degrees.
 */
const toDegrees = (angleRadians: number) => {
  return angleRadians * 180 / Math.PI;
};


/**
 * Returns the coordinate at the given distance and bearing from `c1`.
 *
 * @param {ol.Coordinate} c1 The origin point (`[lon, lat]` in degrees).
 * @param {number} distance The great-circle distance between the origin
 *     point and the target point.
 * @param {number} bearing The bearing (in radians).
 * @return {ol.Coordinate} The target point.
 * 
 * @credits https://stackoverflow.com/a/31696267
 */
const offset = function(c1: [number, number], distance: number, bearing: number): [number, number] {
    var lat1 = toRadians(c1[1]);
    var lon1 = toRadians(c1[0]);
    var dByR = distance / EARTH_RADIUS;
    var lat = Math.asin(
        Math.sin(lat1) * Math.cos(dByR) +
        Math.cos(lat1) * Math.sin(dByR) * Math.cos(bearing)
    );
    var lon = lon1 + Math.atan2(
        Math.sin(bearing) * Math.sin(dByR) * Math.cos(lat1),
        Math.cos(dByR) - Math.sin(lat1) * Math.sin(lat)
    );
    return [
        toDegrees(lon),
        toDegrees(lat),
    ];
};

const pythagorDistance = (c1: [number, number], toCoords: [number, number]) => {
    return Math.sqrt(Math.pow(c1[0] - toCoords[0], 2) + Math.pow(c1[1] - toCoords[1], 2))
}

const getBearingByAtan2 = (fromCoords: [number, number], toCoords: [number, number]) => {
    return Math.atan2(toCoords[0] - fromCoords[0], toCoords[1] - fromCoords[1])
}

const getBearingByTan = (fromCoords: [number, number], toCoords: [number, number]) => {
    return Math.tan((toCoords[1] - fromCoords[1]) / (toCoords[0] - fromCoords[0]))
}

function getBearing1([startLat,startLong], [destLat,destLong]){
    startLat = toRadians(startLat);
    startLong = toRadians(startLong);
    destLat = toRadians(destLat);
    destLong = toRadians(destLong);

    const y = Math.sin(destLong - startLong) * Math.cos(destLat);
    const x = Math.cos(startLat) * Math.sin(destLat) - Math.sin(startLat) * Math.cos(destLat) * Math.cos(destLong - startLong);
    const brng = toDegrees(Math.atan2(y, x));
    return (brng + 360) % 360;
}

function getBearing2([startLat,startLong], [endLat,endLong]){
    startLat = toRadians(startLat);
    startLong = toRadians(startLong);
    endLat = toRadians(endLat);
    endLong = toRadians(endLong);

    var dLong = endLong - startLong;

    var dPhi = Math.log(Math.tan(endLat/2.0+Math.PI/4.0)/Math.tan(startLat/2.0+Math.PI/4.0));
    if (Math.abs(dLong) > Math.PI){
        if (dLong > 0.0)
        dLong = -(2.0 * Math.PI - dLong);
        else
        dLong = (2.0 * Math.PI + dLong);
    }

    return (toDegrees(Math.atan2(dLong, dPhi)) + 360.0) % 360.0;
}

