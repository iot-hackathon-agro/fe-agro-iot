export interface IReportedUnit {
    type: IUnit.Type
    size: IUnit.Size
    activity: IUnit.Activity
    location: [number, number]
    timestamp: number
    additional: IUnit.Additional
    alignment: IUnit.Alignment
}

export interface ITemplateUnit {
    type: IUnit.Type
    size: IUnit.Size
    x: number  // 0 our left (their right), 100 our right (their left).
    y: number  // 0 buttom (front-line), 100 top (their back-line).
    additional: IUnit.Additional
}

export namespace IUnit {

    export type Type =
        | 'air defense'
        | 'anti-tank'
        | 'engineer'
        | 'hq'
        | 'mechanised infantry'
        | 'medics'
        | 'mortars'
        | 'recce'
        | 'self-propelled artillery'
        | 'supply'
        | 'tank'
        // TODO finish Type type.

    export type Size =
        | 'team'
        | 'squad'
        | 'platoon'
        | 'company'
        | 'battalion'

    export type Activity =
        | 'stationary'
        | 'stationary in cover'
        | 'moving N'
        | 'moving NE'
        | 'moving E'
        | 'moving SE'
        | 'moving S'
        | 'moving SW'
        | 'moving W'
        | 'moving NW'

    export type Additional =
        | null
        | 'T-90'
        | 'T-72'

    export type Alignment =
        | 'friend'
        | 'foe'

}
